//
//  ViewController.swift
//  Uebung06
//
//  Created by Peter Gisler on 25.10.17.
//  Copyright © 2017 Peter Gisler HSLU. All rights reserved.
//

import UIKit

class ViewController: UIViewController, XMLParserDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    private var parser: XMLParser?
    
    let dummyStrings: [String] = ["One", "Two", "Three"]
    var pickerStrings: [String] = []
    var tmpXmlStrings: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerStrings = dummyStrings
        
        pickerView.delegate = self
        pickerView.dataSource = self
    }
    
    @IBAction func dataSourceChangePressed(_ sender: UISegmentedControl) {
        switch sender.titleForSegment(at: sender.selectedSegmentIndex) {
        case "XML"?:
            pickerStrings = getXmlData()
            break
        case "JSON"?:
            pickerStrings = getJsonData()
            break
        default:
            pickerStrings = dummyStrings
            break
        }
        
        self.pickerView.reloadAllComponents()
    }
    
    func getXmlData() -> [String] {
        if let url = URL(string: "https://wherever.ch/hslu/iPhoneAdressData.xml") {
            self.parser = XMLParser(contentsOf: url)
            self.parser?.delegate = self
            self.parser?.parse()
        }
        return tmpXmlStrings
    }
    
    func getJsonData() -> [String] {
        var jsonStrings: [String] = []
        if let url = URL(string: "https://wherever.ch/hslu/iPhoneAdressData.json") {
            do {
                let jsonData = try Data(contentsOf: url)
                if let jsonObject = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [[String: Any]] {
                    for person in jsonObject {
                        if let firstName = person["firstName"], let lastName = person["lastName"] {
                            jsonStrings.append("\(firstName) \(lastName)")
                        }
                    }
                }
            } catch {
                print("Error: \(error)")
            }
        }
        return jsonStrings
    }
    
    @IBAction func testOperationQueueButtonPressed(_ sender: UIButton) {
        var orderArray: [String] = []
        
        let blockOp1 = BlockOperation {orderArray.append("1")}
        let blockOp2 = BlockOperation {orderArray.append("2")}
        let blockOp3 = BlockOperation {orderArray.append("3")}
        
        blockOp1.addDependency(blockOp2)
        blockOp1.addDependency(blockOp3)
        blockOp2.addDependency(blockOp3)
        
        let operationQueue = OperationQueue()
        operationQueue.addOperations([blockOp1, blockOp2, blockOp3], waitUntilFinished: true)
        
        let order = orderArray.joined(separator: ", ")
        let alertController = UIAlertController(title: "Block Operation Ordering", message: "The three block operations were executed in the following order: " + order, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: XMLParserDelegate
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        if let firstName = attributeDict["firstName"], let lastName = attributeDict["lastName"] {
            self.tmpXmlStrings.append("\(firstName) \(lastName)")
        }
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerStrings.count
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerStrings[row]
    }
    
}

